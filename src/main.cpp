#include <QApplication>

#include "meshinfo.h"

int main(int argc, char **argv)
{
  QApplication qapp(argc, argv);

  meshinfo app;
  app.show();

  return qapp.exec();
}

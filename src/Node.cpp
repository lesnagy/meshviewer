#include "Node.hpp"

#include "DebugMacros.h"

Node::Node(const Vertex<2> & v0,
           const unsigned int colour[3])
{
  INFO("2D constructor");

  mP0[0] = v0.get(0);
  mP0[1] = v0.get(1);
  mP0[2] = 0.0;

  mColour[0] = colour[0];
  mColour[1] = colour[1];
  mColour[2] = colour[2];

  setup();
}

Node::Node(const Vertex<3> & v0,
           const unsigned int colour[3])
{
  INFO("3D constructor");

  mP0[0] = v0.get(0);
  mP0[1] = v0.get(1);
  mP0[2] = v0.get(2);

  mColour[0] = colour[0];
  mColour[1] = colour[1];
  mColour[2] = colour[2];
  
  setup();
}

void Node::setColour(const unsigned int colour[3])
{
  mColour[0] = colour[0];
  mColour[1] = colour[1];
  mColour[2] = colour[2];

  updateColour();
}

void Node::updateColour()
{
  INFO("Creating mColors (" << 
    mColour[0] << ", " << mColour[1] << ", " << mColour[2] << ")");

  mActor->GetProperty()->SetColor(mColour[0], mColour[1], mColour[2]);
}

void Node::setup()
{
  INFO("Function setup()");

  //Create a sphere
  mSphereSource = vtkSmartPointer<vtkSphereSource>::New();
  mSphereSource->Update();
 
  //Create a mapper and actor
  mMapper = vtkSmartPointer<vtkPolyDataMapper>::New();

  mMapper->SetInputConnection(mSphereSource->GetOutputPort());

  mActor = vtkSmartPointer<vtkActor>::New();
  mActor->SetPosition(mP0[0], mP0[1], mP0[2]);
  mActor->SetScale(0.05);
  mActor->SetMapper(mMapper);

  updateColour();

/*
  INFO("Creating mPoints");
  mPoints = vtkSmartPointer<vtkPoints>::New();
  mPoints->InsertNextPoint(mP0);
  mPoints->InsertNextPoint(mP1);

  INFO("Creating mLinesPolyData");
  mLinesPolyData = vtkSmartPointer<vtkPolyData>::New();
  mLinesPolyData->SetPoints(mPoints);

  INFO("Creating mLine");
  mLine = vtkSmartPointer<vtkLine>::New();
  mLine->GetPointIds()->SetId(0, 0);
  mLine->GetPointIds()->SetId(1, 1);

  INFO("Creating mLines");
  mLines = vtkSmartPointer<vtkCellArray>::New();
  mLines->InsertNextCell(mLine);

  INFO("Adding mLines to mLinesPolyData");
  mLinesPolyData->SetLines(mLines);

  INFO("Creating mMapper");
  mMapper = vtkSmartPointer<vtkPolyDataMapper>::New();
  mMapper->SetInputData(mLinesPolyData);

  INFO("Creating mActor");
  mActor = vtkSmartPointer<vtkActor>::New();
  mActor->SetMapper(mMapper);
  mActor->GetProperty()->SetRepresentationToWireframe();
  mActor->GetProperty()->SetAmbient(1.0);
  mActor->GetProperty()->SetDiffuse(0.0);
  mActor->GetProperty()->SetSpecular(0.0);

  INFO("Creating mColors");
*/
}
#include "TopologyView.hpp"

#include "PatranIOImpl.h"
#include "ElementType.h"

#include "topology/SConn.hpp"

////////////////////////////////////////////////////////////////////////////////
// Constructor                                                                //
////////////////////////////////////////////////////////////////////////////////

TopologyView::TopologyView(vtkSmartPointer<vtkRenderer> renderer)
: mRenderer(renderer)
{
    mViewMode = VERTEX;
    
    mCurrentNodeIndex = 0;
    mShowCurrentNodeActorNeighbours = false;
    
    mCurrentElementIndex = 0;
    mShowCurrentElementActorNeighbours = false;
}

////////////////////////////////////////////////////////////////////////////////
// loadFromFile()                                                             //
////////////////////////////////////////////////////////////////////////////////
void TopologyView::loadFromFile(const std::string & fileName)
{    
  ElementType type = getPatranMeshType(fileName);

  switch (type) {
    case BAR:
      break;
    case QUAD:
      break;
    case WEDGE:
      break;
    case HEX:
      break;
    case TRI:
      populate2D(fileName);
      break;
    case TET:
      populate3D(fileName);
      break;
  }
}

////////////////////////////////////////////////////////////////////////////////
// reset()                                                                    //
////////////////////////////////////////////////////////////////////////////////
void TopologyView::reset()
{
  // TODO: FIX THIS FUNCTION (this class is not currently reentrant!!)
  mUGridActor = NULL;
  mUGridDataSetMapper = NULL;
  mUGrid = NULL;
}

////////////////////////////////////////////////////////////////////////////////
// nextVertex()                                                               //
////////////////////////////////////////////////////////////////////////////////

void TopologyView::nextVertex()
{
    if (mViewMode != VERTEX) return;

    DEBUG("nextVertex()");

    // If the mCurrentNodeIndex is out of range (at the final node) do nothing.
    if (mCurrentNodeIndex == mNodes.size()-1) {
        return;
    }

    DEBUG("settingCurrentNodeIndex to: " << mCurrentNodeIndex+1);

    setCurrentNodeIndex(mCurrentNodeIndex+1);
    updateCurrentNodeNeighbours();
}

////////////////////////////////////////////////////////////////////////////////
// previousVertex()                                                           //
////////////////////////////////////////////////////////////////////////////////

void TopologyView::previousVertex()
{
    if (mViewMode != VERTEX) return;

    DEBUG("previousVertex()");

    // If the mCurrentNodeIndex is out of range (at the final node) do nothing.
    if (mCurrentNodeIndex == 0) {
        return;
    }

    DEBUG("settingCurrentNodeIndex to: " << mCurrentNodeIndex-1);

    setCurrentNodeIndex(mCurrentNodeIndex-1);
    updateCurrentNodeNeighbours();
}

////////////////////////////////////////////////////////////////////////////////
// firstVertex()                                                              //
////////////////////////////////////////////////////////////////////////////////

void TopologyView::firstVertex()
{
    if (mViewMode != VERTEX) return;

    DEBUG("firstVertex()");

    setCurrentNodeIndex(0);
    updateCurrentNodeNeighbours();
}

////////////////////////////////////////////////////////////////////////////////
// lastVertex()                                                               //
////////////////////////////////////////////////////////////////////////////////
void TopologyView::lastVertex()
{
    if (mViewMode != VERTEX) return;
       
    DEBUG("lastVertex()");

    setCurrentNodeIndex(mNodes.size()-1);
    updateCurrentNodeNeighbours();
}

////////////////////////////////////////////////////////////////////////////////
// vertexNeighboursOn()                                                       //
////////////////////////////////////////////////////////////////////////////////

void TopologyView::vertexNeighboursOn()
{
    if (mViewMode != VERTEX) return;

    DEBUG("vertexNeighboursOn()");

    mShowCurrentNodeActorNeighbours = true;
    for (auto actor : mCurrentNodeActorNeighbours) {
        mRenderer->AddActor(actor);
    }
}

////////////////////////////////////////////////////////////////////////////////
// vertexNeighboursOff()                                                      //
////////////////////////////////////////////////////////////////////////////////

void TopologyView::vertexNeighboursOff()
{
    if (mViewMode != VERTEX) return;

    DEBUG("vertexNeighboursOff()");

    mShowCurrentNodeActorNeighbours = false;
    for (auto actor: mCurrentNodeActorNeighbours) {
        mRenderer->RemoveActor(actor);
    }
}

////////////////////////////////////////////////////////////////////////////////
// toggleVertexNeighbours()                                                   //
////////////////////////////////////////////////////////////////////////////////

void TopologyView::toggleVertexNeighbours()
{
    if (mViewMode != VERTEX) return;

    DEBUG("toggleVertexNeighbours()");

    if (mShowCurrentNodeActorNeighbours) {
        vertexNeighboursOff();
    } else {
        vertexNeighboursOn();
    }
}

////////////////////////////////////////////////////////////////////////////////
// nextElement()                                                              //
////////////////////////////////////////////////////////////////////////////////

void TopologyView::nextElement()
{
    if (mViewMode != ELEMENT) return;

    DEBUG("nextElement()");

    // If the mCurrentElementIndex is out of range do nothing.
    if (mCurrentElementIndex == mElements.size()-1) {
        return;
    }

    DEBUG("settingCurrentElementIndex to: " << mCurrentElementIndex+1);
    setCurrentElementIndex(mCurrentElementIndex+1);
    updateCurrentElementNeighbours();
}

////////////////////////////////////////////////////////////////////////////////
// previousElement()                                                          //
////////////////////////////////////////////////////////////////////////////////

void TopologyView::previousElement()
{
    if (mViewMode != ELEMENT) return;

    DEBUG("previousElement()");

    // If the mCurrentElementIndex is out of range, do nothing.
    if (mCurrentElementIndex == 0) {
        return;
    }

    DEBUG("settingCurrentElementIndex to: " << mCurrentElementIndex-1);
    setCurrentElementIndex(mCurrentElementIndex-1);
    updateCurrentElementNeighbours();
}

////////////////////////////////////////////////////////////////////////////////
// firstElement()                                                             //
////////////////////////////////////////////////////////////////////////////////

void TopologyView::firstElement()
{
    if (mViewMode != ELEMENT) return;

    DEBUG("firstElement()");

    setCurrentElementIndex(0);
    updateCurrentElementNeighbours();
}

////////////////////////////////////////////////////////////////////////////////
// lastElement()                                                              //
////////////////////////////////////////////////////////////////////////////////

void TopologyView::lastElement()
{
    if (mViewMode != ELEMENT) return;

    DEBUG("firstElement()");

    setCurrentElementIndex(mElements.size()-1);
    updateCurrentElementNeighbours();
}

////////////////////////////////////////////////////////////////////////////////
// elementNeighboursOn()                                                      //
////////////////////////////////////////////////////////////////////////////////

void TopologyView::elementNeighboursOn()
{
    if (mViewMode != ELEMENT) return;

    DEBUG("elementNeighboursOn()");

    mShowCurrentElementActorNeighbours = true;
    for (auto actor : mCurrentElementActorNeighbours) {
        mRenderer->AddActor(actor);
    }
}

////////////////////////////////////////////////////////////////////////////////
// elementNeighboursOff()                                                     //
////////////////////////////////////////////////////////////////////////////////

void TopologyView::elementNeighboursOff()
{
    if (mViewMode != ELEMENT) return;

    DEBUG("elementNeighboursOff()");

    mShowCurrentElementActorNeighbours = false;
    for (auto actor: mCurrentElementActorNeighbours) {
        mRenderer->RemoveActor(actor);
    }
}

////////////////////////////////////////////////////////////////////////////////
// toggleElementNeighbours()                                                  //
////////////////////////////////////////////////////////////////////////////////

void TopologyView::toggleElementNeighbours()
{
    if (mViewMode != ELEMENT) return;

    DEBUG("toggleElementNeighbours()");

    if (mShowCurrentElementActorNeighbours) {
        elementNeighboursOff();
    } else {
        elementNeighboursOn();
    }
}

////////////////////////////////////////////////////////////////////////////////
// boundaryOn()                                                               //
////////////////////////////////////////////////////////////////////////////////

void TopologyView::boundaryOn()
{}

////////////////////////////////////////////////////////////////////////////////
// boundaryOff()                                                              //
////////////////////////////////////////////////////////////////////////////////

void TopologyView::boundaryOff(){}

////////////////////////////////////////////////////////////////////////////////
// boundaryToggle()                                                           //
////////////////////////////////////////////////////////////////////////////////

void TopologyView::boundaryToggle()
{}

////////////////////////////////////////////////////////////////////////////////
// meshOn()                                                                   //
////////////////////////////////////////////////////////////////////////////////

void TopologyView::meshOn()
{}

////////////////////////////////////////////////////////////////////////////////
// meshOff()                                                                  //
////////////////////////////////////////////////////////////////////////////////

void TopologyView::meshOff()
{}

////////////////////////////////////////////////////////////////////////////////
// meshToggle()                                                               //
////////////////////////////////////////////////////////////////////////////////

void TopologyView::meshToggle()
{}

////////////////////////////////////////////////////////////////////////////////
// switchViewMode()                                                           //
////////////////////////////////////////////////////////////////////////////////

void TopologyView::switchViewMode(ViewMode viewMode)
{
    ViewMode oldViewMode = mViewMode;
    mViewMode = viewMode;

    removeActorsForViewMode(oldViewMode);
    addActorsForViewMode(mViewMode);
}

////////////////////////////////////////////////////////////////////////////////
// removeActorsForViewMode()                                                  //
////////////////////////////////////////////////////////////////////////////////

void TopologyView::removeActorsForViewMode(ViewMode viewMode)
{
    switch (viewMode) {
        case VERTEX:
            DEBUG("Removing vertex actors");
            mRenderer->RemoveActor(mCurrentNodeActor);
            if (mShowCurrentNodeActorNeighbours) {
                for (auto v : mCurrentNodeActorNeighbours) {
                    mRenderer->RemoveActor(v);
                }
            }
            break;
        case ELEMENT:
            DEBUG("Removing element actors");
            mRenderer->RemoveActor(mCurrentElementActor);
            if (mShowCurrentElementActorNeighbours) {
                for (auto v : mCurrentElementActorNeighbours) {
                    mRenderer->RemoveActor(v);
                }
            }
            break;
        case BOUNDARY_VERTEX:
            break;
        case BOUNDARY_ELEMENT:
            break;
    }
}

////////////////////////////////////////////////////////////////////////////////
// addActorsForViewMode()                                                     //
////////////////////////////////////////////////////////////////////////////////

void TopologyView::addActorsForViewMode(ViewMode viewMode)
{
    switch(viewMode) {
        case VERTEX:
            DEBUG("Adding vertex actors");
            setCurrentNodeIndex(mCurrentNodeIndex);
            updateCurrentNodeNeighbours();
            mRenderer->AddActor(mCurrentNodeActor);
            if (mShowCurrentNodeActorNeighbours) {
                for (auto v : mCurrentNodeActorNeighbours) {
                    mRenderer->AddActor(v);
                }
            }
            break;
        case ELEMENT:
            DEBUG("Adding element actors");
            setCurrentElementIndex(mCurrentElementIndex);
            updateCurrentElementNeighbours();
            mRenderer->AddActor(mCurrentElementActor);
            if (mShowCurrentElementActorNeighbours) {
                for (auto v : mCurrentElementActorNeighbours) {
                    mRenderer->AddActor(v);
                }
            }
            break;
        case BOUNDARY_VERTEX:
            break;
        case BOUNDARY_ELEMENT:
            break;
    }
}

////////////////////////////////////////////////////////////////////////////////
// getUGridActor()                                                            //
////////////////////////////////////////////////////////////////////////////////

vtkSmartPointer<vtkActor> TopologyView::getUGridActor()
{
  return mUGridActor;
}

////////////////////////////////////////////////////////////////////////////////
// showElement()                                                              //
////////////////////////////////////////////////////////////////////////////////

void TopologyView::showElement(size_t i)
{}

////////////////////////////////////////////////////////////////////////////////
// hideElement()                                                              //
////////////////////////////////////////////////////////////////////////////////

void TopologyView::hideElement(size_t i)
{}

////////////////////////////////////////////////////////////////////////////////
// populate2D()                                                               //
////////////////////////////////////////////////////////////////////////////////

void loadFromPatran2D(const std::string     & fileName,
                      std::vector<Vertex2D> & vertices,
                      std::vector<Tuple3>   & elements,
                      ElementIndexing         indexing);

void TopologyView::populate2D(const std::string & fileName)
{
  // Delete the stuff we will regenerate
  reset();
  
  std::vector<Vertex2D> vertices;
  std::vector<Tuple3> elements;
  loadFromPatran2D(fileName, vertices, elements, ONE_INDEXING);

  mRenderer->RemoveAllViewProps();

  meshlib::mesh::Mesh<3> mesh;
  mesh.begin();
  for (auto v : vertices) {
    mesh.vertex({v.x, v.y});
  }
  for (size_t i = 0; i < elements.size(); ++i) {
    auto e = elements[i];
    mesh.sconn(
        meshlib::topology::SConn<3>({
            (unsigned int)e.n0, 
            (unsigned int)e.n1, 
            (unsigned int)e.n2}));
  }
  mesh.end();

  const std::vector< meshlib::mesh::Vertex<2> > &  verts = mesh.vertices();
  const meshlib::topology::int_to_intset & vertexNeighbours = mesh.vertexNeighbours();
  const std::vector< meshlib::topology::SConn<3> > & elems = mesh.elements();
  const meshlib::topology::int_to_intset & elementNeighbours = mesh.elementNeighbours();
  const std::vector< meshlib::topology::SConn<2> > & boundaryElements = mesh.boundaryElements();
  const meshlib::topology::int_to_intset & boundaryVertexNeighbours = mesh.boundaryVertexNeighbours();
  const meshlib::topology::int_to_intset & boundaryElementNeighbours = mesh.boundaryElementNeighbours();

  mUGrid = vtkSmartPointer<vtkUnstructuredGrid>::New();

  vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();
  points->SetNumberOfPoints(verts.size());
  for (size_t i = 0; i < verts.size(); ++i) {
    const meshlib::mesh::Vertex<2> & v = verts[i];
    points->SetPoint(i, v[0], v[1], 0.0);
    mNodes.push_back(Node(v, Colours::RED));
  }
  mUGrid->SetPoints(points);

  for (auto kv : vertexNeighbours) {
    for (auto vidx : kv.second) {
        const Vertex<2> v = verts[vidx];
        mNodeNeighbours[kv.first].push_back(Node(v, Colours::BLUE));
    }
  }

  for (size_t i = 0; i < elems.size(); ++i) {
    const meshlib::topology::SConn<3> & e = elems[i];
    DEBUG(e.getc(0) << ", " << e.getc(1) << ", " << e.getc(2));
    vtkIdType element[3] = {e.getc(0), e.getc(1), e.getc(2)};
    mUGrid->InsertNextCell(VTK_TRIANGLE, 3, element);

    const Vertex<2> & ev0 = verts[e.getc(0)];
    const Vertex<2> & ev1 = verts[e.getc(1)];
    const Vertex<2> & ev2 = verts[e.getc(2)];

    mElements.push_back(Triangle(ev0, ev1, ev2, Colours::RED));
  }

  for (auto kv : elementNeighbours) {
    for (auto nidx : kv.second) {
        const meshlib::topology::SConn<3> & ne = elems[nidx];
        Vertex<2> nv0 = verts[ne.getc(0)];
        Vertex<2> nv1 = verts[ne.getc(1)];
        Vertex<2> nv2 = verts[ne.getc(2)];
        mElementNeighbours[kv.first].push_back(Triangle(nv0, nv1, nv2, Colours::BLUE));
    }
  }

  for (size_t i = 0; i < boundaryElements.size(); ++i) {
    const meshlib::topology::SConn<2> & be = boundaryElements[i];
    const Vertex<2> & bev0 = verts[be.getc(0)];
    const Vertex<2> & bev1 = verts[be.getc(1)];
    mBoundaryElements.push_back(Line(bev0, bev1, Colours::RED));
  }

  DEBUG("boundary.size() = " << boundaryElements.size());
  for (auto kv : boundaryElementNeighbours) {
    for (auto nidx : kv.second) {
        DEBUG("Looking for: " << nidx << ", in boundary");
        const meshlib::topology::SConn<2> & nbe = boundaryElements[nidx];
        const Vertex<2> & nv0 = verts[nbe.getc(0)];
        const Vertex<2> & nv1 = verts[nbe.getc(1)];
        mBoundaryElementNeighbours[kv.first].push_back(Line(nv0, nv1, Colours::BLUE));
    }
  }

  for (auto kv: boundaryVertexNeighbours) {
    // Add boundary vertex
    const Vertex<2> & v = verts[kv.first];
    mBoundaryNodeIndices.push_back(kv.first);
    mBoundaryNodes[kv.first] = Node(v, Colours::GREEN);
    for (auto nidx : kv.second) {
        const Vertex<2> & nv = verts[nidx];
        mBoundaryNodeNeighbours[kv.first].push_back(Node(nv, Colours::RED));
    }
  }

  mUGridDataSetMapper = vtkSmartPointer<vtkDataSetMapper>::New();
  mUGridDataSetMapper->SetInputData(mUGrid);
  mUGridDataSetMapper->ScalarVisibilityOff();

  mUGridActor = vtkSmartPointer<vtkActor>::New();
  mUGridActor->SetMapper(mUGridDataSetMapper);
  mUGridActor->GetProperty()->SetRepresentationToWireframe();
  mUGridActor->GetProperty()->SetAmbient(1.0);
  mUGridActor->GetProperty()->SetDiffuse(0.0);
  mUGridActor->GetProperty()->SetSpecular(0.0);

  mRenderer->AddActor(getUGridActor());

  // mRenderer->AddActor(mElements[1].getActor());
  // for (auto t : mElementNeighbours[1]) {
  //   mRenderer->AddActor(t.getActor());
  // }

  // mRenderer->AddActor(mBoundaryElements[0].getActor());
  // for (auto t: mBoundaryElementNeighbours[0]) {
  //   mRenderer->AddActor(t.getActor());
  // }

  // mRenderer->AddActor(mNodes[12].getActor());
  // for (auto t: mNodeNeighbours[12]) {
  //   mRenderer->AddActor(t.getActor());
  // }

  // //for (auto v : mBoundaryNodes) {
  // //  mRenderer->AddActor(v.getActor());
  // //}

  // mBoundaryNodes[mBoundaryNodeIndices[0]].setColour(Colours::BLUE);
  // mRenderer->AddActor(mBoundaryNodes[mBoundaryNodeIndices[0]].getActor());
  // for (auto v : mBoundaryNodeNeighbours[mBoundaryNodeIndices[0]]) {
  //   mRenderer->AddActor(v.getActor());
  // }
  // //mRenderer->AddActor(mBoundaryNodes[0].getActor());

  setCurrentNodeIndex(mCurrentNodeIndex);
  updateCurrentNodeNeighbours();
}

////////////////////////////////////////////////////////////////////////////////
// populate3D()                                                               //
////////////////////////////////////////////////////////////////////////////////

void TopologyView::populate3D(const std::string & fileName)
{}

////////////////////////////////////////////////////////////////////////////////
// setCurrentNodeIndex()                                                      //
////////////////////////////////////////////////////////////////////////////////
void TopologyView::setCurrentNodeIndex(unsigned int idx)
{   
    if (mViewMode != VERTEX) return;

    DEBUG("setCurrentNodeIndex() (index: " << idx << ")");

    mCurrentNodeIndex = idx;
    mRenderer->RemoveActor(mCurrentNodeActor);
    mCurrentNodeActor = mNodes[mCurrentNodeIndex].getActor();
    mRenderer->AddActor(mCurrentNodeActor);
}

////////////////////////////////////////////////////////////////////////////////
// updateCurrentNodeNeighbours()                                              //
////////////////////////////////////////////////////////////////////////////////
void TopologyView::updateCurrentNodeNeighbours()
{
    if (mViewMode != VERTEX) return;

    DEBUG("updateCurrentNodeNeighbours()");
    
    if (mShowCurrentNodeActorNeighbours) {
        for (auto actor : mCurrentNodeActorNeighbours) {
            mRenderer->RemoveActor(actor);
        }
    }
    mCurrentNodeActorNeighbours.clear();
    for (auto actor : mNodeNeighbours[mCurrentNodeIndex]) {
        mCurrentNodeActorNeighbours.push_back(actor.getActor());
    }
    if (mShowCurrentNodeActorNeighbours) {
        for (auto actor : mCurrentNodeActorNeighbours) {
            mRenderer->AddActor(actor);
        }
    }
}

////////////////////////////////////////////////////////////////////////////////
// setCurrentElementIndex()                                                   //
////////////////////////////////////////////////////////////////////////////////
void TopologyView::setCurrentElementIndex(unsigned int idx)
{   
    if (mViewMode != ELEMENT) return;

    DEBUG("setCurrentElementIndex() (index: " << idx << ")");

    mCurrentElementIndex = idx;
    mRenderer->RemoveActor(mCurrentElementActor);
    mCurrentElementActor = mElements[mCurrentElementIndex].getActor();
    mRenderer->AddActor(mCurrentElementActor);
}

////////////////////////////////////////////////////////////////////////////////
// updateCurrentElementNeighbours()                                           //
////////////////////////////////////////////////////////////////////////////////
void TopologyView::updateCurrentElementNeighbours()
{
    if (mViewMode != ELEMENT) return;

    DEBUG("updateCurrentElementNeighbours()");
    
    if (mShowCurrentElementActorNeighbours) {
        for (auto actor : mCurrentElementActorNeighbours) {
            mRenderer->RemoveActor(actor);
        }
    }
    mCurrentElementActorNeighbours.clear();
    for (auto actor : mElementNeighbours[mCurrentElementIndex]) {
        mCurrentElementActorNeighbours.push_back(actor.getActor());
    }
    if (mShowCurrentElementActorNeighbours) {
        for (auto actor : mCurrentElementActorNeighbours) {
            mRenderer->AddActor(actor);
        }
    }
}

#include "Line.hpp"

#include "DebugMacros.h"

Line::Line(const Vertex<2> & v0,
           const Vertex<2> & v1,
           const unsigned int colour[3])
{
  INFO("2D constructor");

  mP0[0] = v0.get(0);
  mP0[1] = v0.get(1);
  mP0[2] = 0.0;

  mP1[0] = v1.get(0);
  mP1[1] = v1.get(1);
  mP1[2] = 0.0;

  mColour[0] = colour[0];
  mColour[1] = colour[1];
  mColour[2] = colour[2];

  setup();
}

Line::Line(const Vertex<3> & v0,
           const Vertex<3> & v1,
           const unsigned int colour[3])
{
  INFO("3D constructor");

  mP0[0] = v0.get(0);
  mP0[1] = v0.get(1);
  mP0[2] = v0.get(2);

  mP1[0] = v1.get(0);
  mP1[1] = v1.get(1);
  mP1[2] = v1.get(2);

  mColour[0] = colour[0];
  mColour[1] = colour[1];
  mColour[2] = colour[2];
  
  setup();
}

void Line::setColour(const unsigned int colour[3])
{
  mColour[0] = colour[0];
  mColour[1] = colour[1];
  mColour[2] = colour[2];

  updateColour();
}

void Line::updateColour()
{
  INFO("Creating mColors (" << 
    mColour[0] << ", " << mColour[1] << ", " << mColour[2] << ")");

  mActor->GetProperty()->SetColor(mColour[0], mColour[1], mColour[2]);
}

void Line::setup()
{
  INFO("Function setup()");

  INFO("Creating mPoints");
  mPoints = vtkSmartPointer<vtkPoints>::New();
  mPoints->InsertNextPoint(mP0);
  mPoints->InsertNextPoint(mP1);

  INFO("Creating mLinesPolyData");
  mLinesPolyData = vtkSmartPointer<vtkPolyData>::New();
  mLinesPolyData->SetPoints(mPoints);

  INFO("Creating mLine");
  mLine = vtkSmartPointer<vtkLine>::New();
  mLine->GetPointIds()->SetId(0, 0);
  mLine->GetPointIds()->SetId(1, 1);

  INFO("Creating mLines");
  mLines = vtkSmartPointer<vtkCellArray>::New();
  mLines->InsertNextCell(mLine);

  INFO("Adding mLines to mLinesPolyData");
  mLinesPolyData->SetLines(mLines);

  INFO("Creating mMapper");
  mMapper = vtkSmartPointer<vtkPolyDataMapper>::New();
  mMapper->SetInputData(mLinesPolyData);

  INFO("Creating mActor");
  mActor = vtkSmartPointer<vtkActor>::New();
  mActor->SetMapper(mMapper);
  mActor->GetProperty()->SetRepresentationToWireframe();
  mActor->GetProperty()->SetAmbient(1.0);
  mActor->GetProperty()->SetDiffuse(0.0);
  mActor->GetProperty()->SetSpecular(0.0);

  INFO("Creating mColors");
  updateColour();
}

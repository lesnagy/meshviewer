#include "Triangle.hpp"

#include "DebugMacros.h"

Triangle::Triangle(const Vertex<2> & v0,
                   const Vertex<2> & v1,
                   const Vertex<2> & v2,
                   const unsigned int colour[3])
{
  mP0[0] = v0.get(0);
  mP0[1] = v0.get(1);
  mP0[2] = 0.0;

  mP1[0] = v1.get(0);
  mP1[1] = v1.get(1);
  mP1[2] = 0.0;

  mP2[0] = v2.get(0);
  mP2[1] = v2.get(1);
  mP2[2] = 0.0;

  mColour[0] = colour[0];
  mColour[1] = colour[1];
  mColour[2] = colour[2];
  
  setup();
}

Triangle::Triangle(const Vertex<3> & v0,
                   const Vertex<3> & v1,
                   const Vertex<3> & v2,
                   const unsigned int colour[3])
{
  mP0[0] = v0.get(0);
  mP0[1] = v0.get(1);
  mP0[2] = v0.get(2);

  mP1[0] = v1.get(0);
  mP1[1] = v1.get(1);
  mP1[2] = v1.get(2);

  mP2[0] = v2.get(0);
  mP2[1] = v2.get(1);
  mP2[2] = v2.get(2);

  mColour[0] = colour[0];
  mColour[1] = colour[1];
  mColour[2] = colour[2];

  setup();
}

void Triangle::setColour(const unsigned int colour[3])
{
  mColour[0] = colour[0];
  mColour[1] = colour[1];
  mColour[2] = colour[2];
  updateColour();
}

void Triangle::updateColour()
{
  INFO("Creating mColors");

  mActor->GetProperty()->SetColor(mColour[0], mColour[1], mColour[2]);
}

void Triangle::setup()
{
  mPoints = vtkSmartPointer<vtkPoints>::New();
  mPoints->InsertNextPoint(mP0);
  mPoints->InsertNextPoint(mP1);
  mPoints->InsertNextPoint(mP2);

  mUGrid = vtkSmartPointer<vtkUnstructuredGrid>::New();
  mUGrid->SetPoints(mPoints);
  vtkIdType ptIdxs[] = {0, 1, 2};
  mUGrid->InsertNextCell(VTK_TRIANGLE, 3, ptIdxs);

  mMapper = vtkSmartPointer<vtkDataSetMapper>::New();
  mMapper->SetInputData(mUGrid);

  mActor = vtkSmartPointer<vtkActor>::New();
  mActor->SetMapper(mMapper);

  updateColour();
  setupNormalArrow();
}

void Triangle::setupNormalArrow()
{
  // Assume mP0 -> mP1 -> mP2 define clockwise winding.
 
  double nx = (-mP0[1] + mP1[1])*(-mP1[2] + mP2[2]) 
    - (-mP0[2] + mP1[2])*(-mP1[1] + mP2[1]);
  double ny = (-mP0[2] + mP1[2])*(-mP1[0] + mP2[0]) 
    - (-mP0[0] + mP1[0])*(-mP1[2] + mP2[2]);
  double nz = (-mP0[0] + mP1[0])*(-mP1[1] + mP2[1]) 
    - (-mP0[1] + mP1[1])*(-mP1[0] + mP2[0]);

  double norm = sqrt(nx*nx + ny*ny + nz*nz);
  nx /= norm;
  ny /= norm;
  nz /= norm;

  double cx = (mP0[0] + mP1[0] + mP2[0])/3.0;
  double cy = (mP0[1] + mP1[1] + mP2[1])/3.0;
  double cz = (mP0[2] + mP1[2] + mP2[2])/3.0;

  double ex = cx + nx;
  double ey = cy + ny;
  double ez = cz + nz;

  Vertex<3> v0({cx, cy, cz});
  Vertex<3> v1({ex, ey, ez});

  mNormalArrow = std::make_shared<Arrow>(v0, v1);
}


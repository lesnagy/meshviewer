#include "Arrow.hpp"

Arrow::Arrow(const Vertex<2> & v0, 
             const Vertex<2> & v1, 
             double            sf)
  : mScale(sf)
{
  mVStart[0] = v0.get(0);
  mVStart[1] = v0.get(1);
  mVStart[2] = 0.0;

  mVEnd[0] = v1.get(0);
  mVEnd[1] = v1.get(1);
  mVEnd[2] = 0.0;

  setup();
}

Arrow::Arrow(const Vertex<3> & v0, 
             const Vertex<3> & v1, 
             double            sf) 
  : mScale(sf)
{
  mVStart[0] = v0.get(0);
  mVStart[1] = v0.get(1);
  mVStart[2] = v0.get(2);

  mVEnd[0] = v1.get(0);
  mVEnd[1] = v1.get(1);
  mVEnd[2] = v1.get(2);

  setup();
}

void Arrow::setup()
{

  double arbitrary[3];

  vtkSmartPointer<vtkMinimalStandardRandomSequence> rand = 
    vtkSmartPointer<vtkMinimalStandardRandomSequence>::New();

  mArrowSource = vtkSmartPointer<vtkArrowSource>::New();

  // X-axis
  vtkMath::Subtract(mVEnd, mVStart, mXAxis);
  mXLength = vtkMath::Norm(mXAxis);
  vtkMath::Normalize(mXAxis);

  // Z-axis
  for (size_t i = 0; i < 3; ++i) {
    rand->Next();
    arbitrary[i] = rand->GetValue();
  }
  vtkMath::Cross(mXAxis, arbitrary, mZAxis);
  mZLength = vtkMath::Norm(mZAxis);
  vtkMath::Normalize(mZAxis);

  // Y-axis
  vtkMath::Cross(mZAxis, mXAxis, mYAxis);
  mYLength = vtkMath::Norm(mYAxis);
  vtkMath::Normalize(mYAxis);

  // Matrix
  vtkSmartPointer<vtkMatrix4x4> M = vtkSmartPointer<vtkMatrix4x4>::New();

  // Fill matrix M with direction cosines
  M->Identity();
  for (size_t i = 0; i < 3; ++i) {
    M->SetElement(i, 0, mXAxis[i]);
    M->SetElement(i, 1, mYAxis[i]);
    M->SetElement(i, 2, mZAxis[i]);
  }

  mTransform = vtkSmartPointer<vtkTransform>::New();
  mTransform->Translate(mVStart);
  mTransform->Concatenate(M);
  mTransform->Scale(mScale, mScale, mScale);

  mTransformPolyDataFilter = vtkSmartPointer<vtkTransformPolyDataFilter>::New();
  mTransformPolyDataFilter->SetTransform(mTransform);
  mTransformPolyDataFilter->SetInputConnection(mArrowSource->GetOutputPort());
  mTransformPolyDataFilter->Update();

  mPolyDataMapper = vtkSmartPointer<vtkPolyDataMapper>::New();

  mActor = vtkSmartPointer<vtkActor>::New();

  mPolyDataMapper->SetInputConnection(mTransformPolyDataFilter->GetOutputPort());
  mPolyDataMapper->Update();

  mActor->SetMapper(mPolyDataMapper);
}

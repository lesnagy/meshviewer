#ifndef TOPOLOGY_VIEW
#define TOPOLOGY_VIEW

#include <string>
#include <vector>
#include <unordered_map>
#include <set>

#include <vtkActor.h>
#include <vtkArrowSource.h>
#include <vtkCellType.h>
#include <vtkDataSetMapper.h>
#include <vtkMath.h>
#include <vtkMatrix4x4.h>
#include <vtkMinimalStandardRandomSequence.h>
#include <vtkPoints.h>
#include <vtkPolyDataMapper.h>
#include <vtkProperty.h>
#include <vtkRenderer.h>
#include <vtkSmartPointer.h>
#include <vtkTransform.h>
#include <vtkTransformPolyDataFilter.h>
#include <vtkUnstructuredGrid.h>

#include "Node.hpp"
#include "Arrow.hpp"
#include "Line.hpp"
#include "Triangle.hpp"
#include "Tetrahedron.hpp"

#include "mesh/Mesh.hpp"
#include "Vertex2D.hpp"
#include "Tuple3.hpp"

enum ViewMode
{
  VERTEX,
  ELEMENT,
  BOUNDARY_VERTEX,
  BOUNDARY_ELEMENT
};

class TopologyView
{
public:
  TopologyView(vtkSmartPointer<vtkRenderer> renderer);

  void loadFromFile(const std::string & fileName);

  void reset();

  void nextVertex();

  void previousVertex();

  void firstVertex();

  void lastVertex();

  void vertexNeighboursOn();

  void vertexNeighboursOff();

  void toggleVertexNeighbours();

  void nextElement();

  void previousElement();

  void firstElement();

  void lastElement();

  void elementNeighboursOn();

  void elementNeighboursOff();

  void toggleElementNeighbours();

  void boundaryOn();

  void boundaryOff();

  void boundaryToggle();

  void meshOn();

  void meshOff();

  void meshToggle();

  void switchViewMode(ViewMode viewMode);

  void removeActorsForViewMode(ViewMode viewMode);

  void addActorsForViewMode(ViewMode viewMode);

  vtkSmartPointer<vtkActor> getUGridActor();

private:
  vtkSmartPointer<vtkRenderer> mRenderer;

  std::vector<Element> mNodes;
  std::unordered_map< unsigned int, std::vector<Element> > mNodeNeighbours;

  vtkSmartPointer<vtkUnstructuredGrid> mUGrid;
  vtkSmartPointer<vtkDataSetMapper> mUGridDataSetMapper;
  vtkSmartPointer<vtkActor> mUGridActor;

  std::vector< Element > mElements;
  std::unordered_map< unsigned int, std::vector<Element> > mElementNeighbours;

  std::vector< unsigned int > mBoundaryNodeIndices;
  std::unordered_map< unsigned int, Element > mBoundaryNodes;
  std::unordered_map< unsigned int, std::vector<Element> > mBoundaryNodeNeighbours;

  std::vector< Element > mBoundaryElements;
  std::unordered_map< unsigned int, std::vector<Element> > mBoundaryElementNeighbours;
  std::vector< Arrow > mBoundaryElementNormals;

  ViewMode mViewMode;

  //////////////////////////////////////////////////////////////////////////////
  // Variables active when ViewMode is VERTEX                                 //
  //////////////////////////////////////////////////////////////////////////////

  // The current active node
  vtkSmartPointer<vtkActor> mCurrentNodeActor;

  // The index of the current node/vertex being shown.
  unsigned int mCurrentNodeIndex;

  // List of current active neighbours
  std::vector< vtkSmartPointer<vtkActor> > mCurrentNodeActorNeighbours;

  // Flag to see whether vertex neighbours are on/off
  bool mShowCurrentNodeActorNeighbours;

  //////////////////////////////////////////////////////////////////////////////
  // Variables acive when ViewMode is ELEMENT                                 //
  //////////////////////////////////////////////////////////////////////////////

  // The current active element
  vtkSmartPointer<vtkActor> mCurrentElementActor;

  // The index of the current element being shown.
  unsigned int mCurrentElementIndex;

  // List of current active element-neighbours
  std::vector< vtkSmartPointer<vtkActor> > mCurrentElementActorNeighbours;

  // Flag to see whether vertex neighbours are on/off
  bool mShowCurrentElementActorNeighbours;

  void showElement(size_t i);

  void hideElement(size_t i);

  void populate2D(const std::string & fileName);

  void populate3D(const std::string & fileName);

  void setCurrentNodeIndex(unsigned int idx);

  void updateCurrentNodeNeighbours();

  void setCurrentElementIndex(unsigned int idx);

  void updateCurrentElementNeighbours();

};

#endif // TOPOLOGY_VIEW

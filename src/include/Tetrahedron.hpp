#ifndef TETRAHEDRON_HPP
#define TETRAHEDRON_HPP

#include <vtkActor.h>
#include <vtkArrowSource.h>
#include <vtkCellType.h>
#include <vtkDataSetMapper.h>
#include <vtkMath.h>
#include <vtkMatrix4x4.h>
#include <vtkMinimalStandardRandomSequence.h>
#include <vtkPoints.h>
#include <vtkPolyDataMapper.h>
#include <vtkProperty.h>
#include <vtkRenderer.h>
#include <vtkSmartPointer.h>
#include <vtkTransform.h>
#include <vtkTransformPolyDataFilter.h>
#include <vtkUnstructuredGrid.h>

#include "mesh/Vertex.hpp"

#include "Element.hpp"

template <unsigned int N>
using Vertex = meshlib::mesh::Vertex<N>;

class Tetrahedron : public Element
{
public:
  Tetrahedron(const Vertex<3> & v0, 
              const Vertex<3> & v1, 
              const Vertex<3> & v2, 
              const Vertex<3> & v3);

private:
  double mP0[3];
  double mP1[3];
  double mP2[3];
  double mP3[3];

  vtkSmartPointer<vtkPoints>           mPoints;
  vtkSmartPointer<vtkUnstructuredGrid> mUGrid;
  vtkSmartPointer<vtkDataSetMapper>    mMapper;

  void setup();
};

#endif // TETRAHEDRON_HPP

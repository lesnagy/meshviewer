#ifndef MESHINFO_H
#define MESHINFO_H

#include <vector>
#include <algorithm>
#include <utility>

#include <vtkActor.h>
#include <vtkArrayCalculator.h>
#include <vtkArrowSource.h>
#include <vtkCellData.h>
#include <vtkColorTransferFunction.h>
#include <vtkCubeSource.h>
#include <vtkDataObjectToTable.h>
#include <vtkDataSetMapper.h>
#include <vtkDoubleArray.h>
#include <vtkElevationFilter.h>
#include <vtkFloatArray.h>
#include <vtkGlyph3D.h>
#include <vtkGradientFilter.h>
#include <vtkLookupTable.h>
#include <vtkMaskPoints.h>
#include <vtkPointData.h>
#include <vtkPolyDataMapper.h>
#include <vtkProperty.h>
#include <vtkQtTableView.h>
#include <vtkRenderWindow.h>
#include <vtkRenderer.h>
#include <vtkSmartPointer.h>
#include <vtkSphereSource.h>
#include <vtkTransform.h>
#include <vtkTransformPolyDataFilter.h>
#include <vtkUnstructuredGrid.h>

#include <QFileDialog>
#include <QMainWindow>
#include <QMessageBox>

#include <ui_meshinfo.h>

#include "DebugMacros.h"
#include "mesh/mesh.hpp"

#include "Arrow.hpp"
#include "Line.hpp"
#include "Triangle.hpp"
#include "Tetrahedron.hpp"
#include "TopologyView.hpp"

class meshinfo : public QMainWindow, private Ui::meshinfo
{
  Q_OBJECT
public:

  // Constructor/Destructor
  meshinfo(); 
  ~meshinfo();

public slots:

  // Element/vertex navigation.
  virtual void slotVertexNavigationGroupBoxClicked(bool checked);
  virtual void slotVertexNeighboursCheckBoxClicked(bool checked);
  virtual void slotFirstVertexButtonClicked();
  virtual void slotPreviousVertexButtonClicked();
  virtual void slotNextVertexButtonClicked();
  virtual void slotLastVertexButtonClicked();

  virtual void slotElementNavigationGroupBoxClicked(bool checked);
  virtual void slotElementNeighboursCheckBoxClicked(bool checked);
  virtual void slotFirstElementButtonClicked();
  virtual void slotPreviousElementButtonClicked();
  virtual void slotNextElementButtonClicked();
  virtual void slotLastElementButtonClicked();

  virtual void slotBoundaryVertexNavigationGroupBoxClicked(bool checked);
  virtual void slotBoundaryVertexNeighboursCheckBoxClicked(bool checked);
  virtual void slotFirstBoundaryVertexButtonClicked();
  virtual void slotPreviousBoundaryVertexButtonClicked();
  virtual void slotNextBoundaryVertexButtonClicked();
  virtual void slotLastBoundaryVertexButtonClicked();

  virtual void slotBoundaryFaceNavigationGroupBoxClicked(bool checked);
  virtual void slotBoundaryFaceNeighboursCheckBoxClicked(bool checked);
  virtual void slotFirstBoundaryFaceButtonClicked();
  virtual void slotPreviousBoundaryFaceButtonClicked();
  virtual void slotNextBoundaryFaceButtonClicked();
  virtual void slotLastBoundaryFaceButtonClicked();

private:
  QGroupBox* mCurrentActiveNavigationGroupBox;
  QCheckBox* mCurrentActiveNeighboursCheckBox;

  // Renderer
  vtkSmartPointer<vtkRenderer> mRenderer;

  std::shared_ptr<TopologyView> mTopology;

  // std::shared_ptr<Arrow> mArrow;
  // std::shared_ptr<Line> mLine;
  // std::shared_ptr<Triangle> mTriangle;
  // std::shared_ptr<Tetrahedron> mTetrahedron;
};

#endif

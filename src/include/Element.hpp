#ifndef ELEMENT_HPP_
#define ELEMENT_HPP_

class Element
{
public:
  const vtkSmartPointer<vtkActor> & getActor() { return mActor; }

  void setColour(const unsigned int colour[3]) {
    mActor->GetProperty()->SetColor(colour[0], colour[1], colour[2]);
  }

protected:
  vtkSmartPointer<vtkActor> mActor;
};

#endif  // ELEMENT_HPP_
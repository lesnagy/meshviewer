#ifndef COLOURS_HPP_
#define COLOURS_HPP_

class Colours
{
public:
  const static unsigned int ALICE_BLUE[3];
  const static unsigned int ANTIQUE_WHITE[3];
  const static unsigned int AQUA[3];
  const static unsigned int AQUAMARINE[3];
  const static unsigned int AZURE[3];
  const static unsigned int BEIGE[3];
  const static unsigned int BISQUE[3];
  const static unsigned int BLACK[3];
  const static unsigned int BLANCHED_ALMOND[3];
  const static unsigned int BLUE[3];
  const static unsigned int BLUE_VIOLET[3];
  const static unsigned int BROWN[3];
  const static unsigned int BURLYWOOD[3];
  const static unsigned int CADET_BLUE[3];
  const static unsigned int CHARTREUSE[3];
  const static unsigned int CHOCOLATE[3];
  const static unsigned int CORAL[3];
  const static unsigned int CORNFLOWER[3];
  const static unsigned int CORNSILK[3];
  const static unsigned int CRIMSON[3];
  const static unsigned int CYAN[3];
  const static unsigned int DARK_BLUE[3];
  const static unsigned int DARK_CYAN[3];
  const static unsigned int DARK_GOLDENROD[3];
  const static unsigned int DARK_GRAY[3];
  const static unsigned int DARK_GREEN[3];
  const static unsigned int DARK_KHAKI[3];
  const static unsigned int DARK_MAGENTA[3];
  const static unsigned int DARK_OLIVE_GREEN[3];
  const static unsigned int DARK_ORANGE[3];
  const static unsigned int DARK_ORCHID[3];
  const static unsigned int DARK_RED[3];
  const static unsigned int DARK_SALMON[3];
  const static unsigned int DARK_SEA_GREEN[3];
  const static unsigned int DARK_SLATE_BLUE[3];
  const static unsigned int DARK_SLATE_GRAY[3];
  const static unsigned int DARK_TURQUOISE[3];
  const static unsigned int DARK_VIOLET[3];
  const static unsigned int DEEP_PINK[3];
  const static unsigned int DEEP_SKY_BLUE[3];
  const static unsigned int DIM_GRAY[3];
  const static unsigned int DODGER_BLUE[3];
  const static unsigned int FIREBRICK[3];
  const static unsigned int FLORAL_WHITE[3];
  const static unsigned int FOREST_GREEN[3];
  const static unsigned int FUCHSIA[3];
  const static unsigned int GAINSBORO[3];
  const static unsigned int GHOST_WHITE[3];
  const static unsigned int GOLD[3];
  const static unsigned int GOLDENROD[3];
  const static unsigned int GRAY[3];
  const static unsigned int WEB_GRAY[3];
  const static unsigned int GREEN[3];
  const static unsigned int WEB_GREEN[3];
  const static unsigned int GREEN_YELLOW[3];
  const static unsigned int HONEYDEW[3];
  const static unsigned int HOT_PINK[3];
  const static unsigned int INDIAN_RED[3];
  const static unsigned int INDIGO[3];
  const static unsigned int IVORY[3];
  const static unsigned int KHAKI[3];
  const static unsigned int LAVENDER[3];
  const static unsigned int LAVENDER_BLUSH[3];
  const static unsigned int LAWN_GREEN[3];
  const static unsigned int LEMON_CHIFFON[3];
  const static unsigned int LIGHT_BLUE[3];
  const static unsigned int LIGHT_CORAL[3];
  const static unsigned int LIGHT_CYAN[3];
  const static unsigned int LIGHT_GOLDENROD[3];
  const static unsigned int LIGHT_GRAY[3];
  const static unsigned int LIGHT_GREEN[3];
  const static unsigned int LIGHT_PINK[3];
  const static unsigned int LIGHT_SALMON[3];
  const static unsigned int LIGHT_SEA_GREEN[3];
  const static unsigned int LIGHT_SKY_BLUE[3];
  const static unsigned int LIGHT_SLATE_GRAY[3];
  const static unsigned int LIGHT_STEEL_BLUE[3];
  const static unsigned int LIGHT_YELLOW[3];
  const static unsigned int LIME[3];
  const static unsigned int LIME_GREEN[3];
  const static unsigned int LINEN[3];
  const static unsigned int MAGENTA[3];
  const static unsigned int MAROON[3];
  const static unsigned int WEB_MAROON[3];
  const static unsigned int MEDIUM_AQUAMARINE[3];
  const static unsigned int MEDIUM_BLUE[3];
  const static unsigned int MEDIUM_ORCHID[3];
  const static unsigned int MEDIUM_PURPLE[3];
  const static unsigned int MEDIUM_SEA_GREEN[3];
  const static unsigned int MEDIUM_SLATE_BLUE[3];
  const static unsigned int MEDIUM_SPRING_GREEN[3];
  const static unsigned int MEDIUM_TURQUOISE[3];
  const static unsigned int MEDIUM_VIOLET_RED[3];
  const static unsigned int MIDNIGHT_BLUE[3];
  const static unsigned int MINT_CREAM[3];
  const static unsigned int MISTY_ROSE[3];
  const static unsigned int MOCCASIN[3];
  const static unsigned int NAVAJO_WHITE[3];
  const static unsigned int NAVY_BLUE[3];
  const static unsigned int OLD_LACE[3];
  const static unsigned int OLIVE[3];
  const static unsigned int OLIVE_DRAB[3];
  const static unsigned int ORANGE[3];
  const static unsigned int ORANGE_RED[3];
  const static unsigned int ORCHID[3];
  const static unsigned int PALE_GOLDENROD[3];
  const static unsigned int PALE_GREEN[3];
  const static unsigned int PALE_TURQUOISE[3];
  const static unsigned int PALE_VIOLET_RED[3];
  const static unsigned int PAPAYA_WHIP[3];
  const static unsigned int PEACH_PUFF[3];
  const static unsigned int PERU[3];
  const static unsigned int PINK[3];
  const static unsigned int PLUM[3];
  const static unsigned int POWDER_BLUE[3];
  const static unsigned int PURPLE[3];
  const static unsigned int WEB_PURPLE[3];
  const static unsigned int REBECCA_PURPLE[3];
  const static unsigned int RED[3];
  const static unsigned int ROSY_BROWN[3];
  const static unsigned int ROYAL_BLUE[3];
  const static unsigned int SADDLE_BROWN[3];
  const static unsigned int SALMON[3];
  const static unsigned int SANDY_BROWN[3];
  const static unsigned int SEA_GREEN[3];
  const static unsigned int SEASHELL[3];
  const static unsigned int SIENNA[3];
  const static unsigned int SILVER[3];
  const static unsigned int SKY_BLUE[3];
  const static unsigned int SLATE_BLUE[3];
  const static unsigned int SLATE_GRAY[3];
  const static unsigned int SNOW[3];
  const static unsigned int SPRING_GREEN[3];
  const static unsigned int STEEL_BLUE[3];
  const static unsigned int TAN[3];
  const static unsigned int TEAL[3];
  const static unsigned int THISTLE[3];
  const static unsigned int TOMATO[3];
  const static unsigned int TURQUOISE[3];
  const static unsigned int VIOLET[3];
  const static unsigned int WHEAT[3];
  const static unsigned int WHITE[3];
  const static unsigned int WHITE_SMOKE[3];
  const static unsigned int YELLOW[3];
  const static unsigned int YELLOW_GREEN[3];
};

#endif // COLOURS_HPP_
#ifndef NODE_HPP_
#define NODE_HPP_

#include <vtkActor.h>
#include <vtkArrowSource.h>
#include <vtkCellData.h>
#include <vtkCellType.h>
#include <vtkDataSetMapper.h>
#include <vtkLine.h>
#include <vtkMath.h>
#include <vtkMatrix4x4.h>
#include <vtkMinimalStandardRandomSequence.h>
#include <vtkPoints.h>
#include <vtkPolyDataMapper.h>
#include <vtkProperty.h>
#include <vtkRenderer.h>
#include <vtkSmartPointer.h>
#include <vtkTransform.h>
#include <vtkTransformPolyDataFilter.h>
#include <vtkUnsignedCharArray.h>
#include <vtkUnstructuredGrid.h>
#include <vtkSphereSource.h>

#include "mesh/Vertex.hpp"

#include "Colours.hpp"
#include "Element.hpp"

template <unsigned int N>
using Vertex = meshlib::mesh::Vertex<N>;

class Node : public Element
{
public:
  Node(const Vertex<2> & v0, 
       const unsigned int colour[3]);

  Node(const Vertex<3> & v0, 
       const unsigned int colour[3]);

  void setColour(const unsigned int colour[3]);

private:
  double mP0[3];

  unsigned int mColour[3];


  vtkSmartPointer<vtkSphereSource>   mSphereSource;

  vtkSmartPointer<vtkPoints>         mPoints;
  vtkSmartPointer<vtkPolyDataMapper> mMapper;  

  void setup();
  void updateColour();
};


#endif  // NODE_HPP_
#ifndef TRIANGLE_HPP
#define TRIANGLE_HPP

#include <vtkActor.h>
#include <vtkArrowSource.h>
#include <vtkCellType.h>
#include <vtkDataSetMapper.h>
#include <vtkMath.h>
#include <vtkMatrix4x4.h>
#include <vtkMinimalStandardRandomSequence.h>
#include <vtkPoints.h>
#include <vtkPolyDataMapper.h>
#include <vtkProperty.h>
#include <vtkRenderer.h>
#include <vtkSmartPointer.h>
#include <vtkTransform.h>
#include <vtkTransformPolyDataFilter.h>
#include <vtkUnstructuredGrid.h>

#include "mesh/Vertex.hpp"

#include "Colours.hpp"
#include "Element.hpp"
#include "Arrow.hpp"

template <unsigned int N>
using Vertex = meshlib::mesh::Vertex<N>;

class Triangle : public Element
{
public:
  Triangle(const Vertex<2> & v0, 
           const Vertex<2> & v1, 
           const Vertex<2> & v2,
           const unsigned int colour[3]);

  Triangle(const Vertex<3> & v0, 
           const Vertex<3> & v1, 
           const Vertex<3> & v2,
           const unsigned int colour[3]);

  void setColour(const unsigned int colour[3]);

  const vtkSmartPointer<vtkActor> & getNormalActor()
  { 
    return mNormalArrow->getActor(); 
  }

private:

  double mP0[3];
  double mP1[3];
  double mP2[3];

  unsigned int mColour[3];

  vtkSmartPointer<vtkPoints>           mPoints;
  vtkSmartPointer<vtkUnstructuredGrid> mUGrid;
  vtkSmartPointer<vtkDataSetMapper>    mMapper;

  std::shared_ptr<Arrow>               mNormalArrow;

  void setup();
  void setupNormalArrow();
  void updateColour();
};

#endif // TRIANGLE_HPP

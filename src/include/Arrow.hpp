#ifndef ARROW_HPP
#define ARROW_HPP

#include <vtkActor.h>
#include <vtkArrowSource.h>
#include <vtkCellType.h>
#include <vtkDataSetMapper.h>
#include <vtkMath.h>
#include <vtkMatrix4x4.h>
#include <vtkMinimalStandardRandomSequence.h>
#include <vtkPoints.h>
#include <vtkPolyDataMapper.h>
#include <vtkProperty.h>
#include <vtkRenderer.h>
#include <vtkSmartPointer.h>
#include <vtkTransform.h>
#include <vtkTransformPolyDataFilter.h>
#include <vtkUnstructuredGrid.h>

#include "mesh/Vertex.hpp"

template <unsigned int N>
using Vertex = meshlib::mesh::Vertex<N>;

class Arrow
{
public:
  Arrow(const Vertex<2> & v0, const Vertex<2> & v1, double scale=1.0);
  Arrow(const Vertex<3> & v0, const Vertex<3> & v1, double scale=1.0);

  const vtkSmartPointer<vtkActor> & getActor() 
  { 
    return mActor; 
  }

private:
  double mVStart[3];
  double mVEnd[3];
  double mXAxis[3];
  double mXLength;
  double mYAxis[3];
  double mYLength;
  double mZAxis[3];
  double mZLength;

  double mScale;

  vtkSmartPointer<vtkArrowSource>             mArrowSource;
  vtkSmartPointer<vtkTransform>               mTransform;
  vtkSmartPointer<vtkTransformPolyDataFilter> mTransformPolyDataFilter;
  vtkSmartPointer<vtkPolyDataMapper>          mPolyDataMapper;
  vtkSmartPointer<vtkActor>                   mActor;

  void setup();
};

#endif // ARROW_HPP

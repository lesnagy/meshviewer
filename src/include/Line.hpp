#ifndef LINE_HPP
#define LINE_HPP

#include <vtkActor.h>
#include <vtkArrowSource.h>
#include <vtkCellData.h>
#include <vtkCellType.h>
#include <vtkDataSetMapper.h>
#include <vtkLine.h>
#include <vtkMath.h>
#include <vtkMatrix4x4.h>
#include <vtkMinimalStandardRandomSequence.h>
#include <vtkPoints.h>
#include <vtkPolyDataMapper.h>
#include <vtkProperty.h>
#include <vtkRenderer.h>
#include <vtkSmartPointer.h>
#include <vtkTransform.h>
#include <vtkTransformPolyDataFilter.h>
#include <vtkUnsignedCharArray.h>
#include <vtkUnstructuredGrid.h>

#include "mesh/Vertex.hpp"

#include "Colours.hpp"
#include "Element.hpp"

template <unsigned int N>
using Vertex = meshlib::mesh::Vertex<N>;

class Line : public Element
{
public:
  Line(const Vertex<2> & v0, 
       const Vertex<2> & v1, 
       const unsigned int colour[3]);

  Line(const Vertex<3> & v0, 
       const Vertex<3> & v1, 
       const unsigned int colour[3]);

  void setColour(const unsigned int colour[3]);

private:
  double mP0[3];
  double mP1[3];

  unsigned int mColour[3];

  vtkSmartPointer<vtkPoints>            mPoints;
  vtkSmartPointer<vtkPolyData>          mLinesPolyData;
  vtkSmartPointer<vtkLine>              mLine;
  //vtkSmartPointer<vtkUnsignedCharArray> mColors;
  vtkSmartPointer<vtkCellArray>         mLines;
  vtkSmartPointer<vtkPolyDataMapper>    mMapper;

  void setup();
  void updateColour();
};

#endif // LINE_HPP

#include "Tetrahedron.hpp"

Tetrahedron::Tetrahedron(const Vertex<3> & v0, 
                         const Vertex<3> & v1, 
                         const Vertex<3> & v2, 
                         const Vertex<3> & v3)
{
  mP0[0] = v0.get(0);
  mP0[1] = v0.get(1);
  mP0[2] = v0.get(2);

  mP1[0] = v1.get(0);
  mP1[1] = v1.get(1);
  mP1[2] = v1.get(2);

  mP2[0] = v2.get(0);
  mP2[1] = v2.get(1);
  mP2[2] = v2.get(2);

  mP3[0] = v3.get(0);
  mP3[1] = v3.get(1);
  mP3[2] = v3.get(2);

  setup();
}

void Tetrahedron::setup()
{
  mPoints = vtkSmartPointer<vtkPoints>::New();
  mPoints->InsertNextPoint(mP0);
  mPoints->InsertNextPoint(mP1);
  mPoints->InsertNextPoint(mP2);
  mPoints->InsertNextPoint(mP3);

  mUGrid = vtkSmartPointer<vtkUnstructuredGrid>::New();
  mUGrid->SetPoints(mPoints);
  vtkIdType ptIdxs[] = {0, 1, 2, 3};
  mUGrid->InsertNextCell(VTK_TETRA, 4, ptIdxs);

  mMapper = vtkSmartPointer<vtkDataSetMapper>::New();
  mMapper->SetInputData(mUGrid);

  mActor = vtkSmartPointer<vtkActor>::New();
  mActor->SetMapper(mMapper);
}

#include "meshinfo.h"

// Constructor
meshinfo::meshinfo() 
{
  setupUi(this);

  mRenderer = vtkSmartPointer<vtkRenderer>::New();
  mVTKView->GetRenderWindow()->AddRenderer(mRenderer);

  mTopology = std::make_shared<TopologyView>(mRenderer);

  mTopology->loadFromFile("/Users/lesnagy/surface.pat");
  
  connect(mVertexNavigationGroupBox, 
          SIGNAL(clicked(bool)), 
          this, 
          SLOT(slotVertexNavigationGroupBoxClicked(bool)));

  connect(mVertexNeighboursCheckBox,
          SIGNAL(clicked(bool)),
          this,
          SLOT(slotVertexNeighboursCheckBoxClicked(bool)));

  connect(mFirstVertexButton,
          SIGNAL(clicked()), 
          this, 
          SLOT(slotFirstVertexButtonClicked()));

  connect(mPreviousVertexButton,
          SIGNAL(clicked()), 
          this, 
          SLOT(slotPreviousVertexButtonClicked()));

  connect(mNextVertexButton,
          SIGNAL(clicked()), 
          this, 
          SLOT(slotNextVertexButtonClicked()));
  
  connect(mLastVertexButton,
          SIGNAL(clicked()), 
          this, 
          SLOT(slotLastVertexButtonClicked()));

  connect(mElementNavigationGroupBox,
          SIGNAL(clicked(bool)),
          this,
          SLOT(slotElementNavigationGroupBoxClicked(bool)));

  connect(mElementNeighboursCheckBox,
          SIGNAL(clicked(bool)),
          this,
          SLOT(slotElementNeighboursCheckBoxClicked(bool)));

  connect(mFirstElementButton,
          SIGNAL(clicked()), 
          this, 
          SLOT(slotFirstElementButtonClicked()));

  connect(mPreviousElementButton,
          SIGNAL(clicked()), 
          this, 
          SLOT(slotPreviousElementButtonClicked()));

  connect(mNextElementButton,
          SIGNAL(clicked()), 
          this, 
          SLOT(slotNextElementButtonClicked()));

  connect(mLastElementButton,
          SIGNAL(clicked()),
          this,
          SLOT(slotLastElementButtonClicked()));

  connect(mBoundaryVertexNavigationGroupBox,
          SIGNAL(clicked(bool)),
          this,
          SLOT(slotBoundaryVertexNavigationGroupBoxClicked(bool)));

  connect(mBoundaryVertexNeighboursCheckBox,
          SIGNAL(clicked(bool)),
          this,
          SLOT(slotBoundaryVertexNeighboursCheckBoxClicked(bool)));

  connect(mFirstBoundaryVertexButton,
          SIGNAL(clicked()), 
          this, 
          SLOT(slotFirstBoundaryVertexButtonClicked()));

  connect(mPreviousBoundaryVertexButton,
          SIGNAL(clicked()), 
          this, 
          SLOT(slotPreviousBoundaryVertexButtonClicked()));

  connect(mNextBoundaryVertexButton,
          SIGNAL(clicked()), 
          this, 
          SLOT(slotNextBoundaryVertexButtonClicked()));

  connect(mLastBoundaryVertexButton,
          SIGNAL(clicked()), 
          this, 
          SLOT(slotLastBoundaryVertexButtonClicked()));

  connect(mBoundaryFaceNavigationGroupBox,
          SIGNAL(clicked(bool)),
          this,
          SLOT(slotBoundaryFaceNavigationGroupBoxClicked(bool)));

  connect(mBoundaryFaceNeighboursCheckBox,
          SIGNAL(clicked(bool)),
          this,
          SLOT(slotBoundaryFaceNeighboursCheckBoxClicked(bool)));

  connect(mFirstBoundaryFaceButton,
          SIGNAL(clicked()), 
          this, 
          SLOT(slotFirstBoundaryFaceButtonClicked()));

  connect(mPreviousBoundaryFaceButton,
          SIGNAL(clicked()), 
          this, 
          SLOT(slotPreviousBoundaryFaceButtonClicked()));

  connect(mNextBoundaryFaceButton,
          SIGNAL(clicked()), 
          this, 
          SLOT(slotNextBoundaryFaceButtonClicked()));
  
  connect(mLastBoundaryFaceButton,
          SIGNAL(clicked()), 
          this, 
          SLOT(slotLastBoundaryFaceButtonClicked()));

  mCurrentActiveNavigationGroupBox = mVertexNavigationGroupBox;
  mCurrentActiveNeighboursCheckBox = NULL;
}

// Destructor
meshinfo::~meshinfo()
{}

///////////////////////////////////////////////////////////////////////////////
// Vertex navigation                                                         //
///////////////////////////////////////////////////////////////////////////////

void meshinfo::slotVertexNavigationGroupBoxClicked(bool checked)
{
  INFO("slotVertexNavigationGroupBoxClicked()");
  if (mCurrentActiveNavigationGroupBox != mVertexNavigationGroupBox) {
    INFO("   not currently active... activating");
    mCurrentActiveNavigationGroupBox->setChecked(false);
    mCurrentActiveNavigationGroupBox = mVertexNavigationGroupBox;

    mVertexNeighboursCheckBox->setEnabled(true);
    mElementNeighboursCheckBox->setEnabled(false);
    mBoundaryVertexNeighboursCheckBox->setEnabled(false);
    mBoundaryFaceNeighboursCheckBox->setEnabled(false);

    mTopology->switchViewMode(VERTEX);
    mVTKView->update();
  } else {
    INFO("   is currently active... ignoring");
    mCurrentActiveNavigationGroupBox->setChecked(true);
  }
}

void meshinfo::slotVertexNeighboursCheckBoxClicked(bool checked)
{
  INFO("slotVertexNeighboursCheckBoxClicked()");
  mTopology->toggleVertexNeighbours();
  mVTKView->update();
}

void meshinfo::slotFirstVertexButtonClicked()
{
  INFO("slotFirstVertexButtonClicked()");
  mTopology->firstVertex();
  mVTKView->update();
}

void meshinfo::slotPreviousVertexButtonClicked()
{
  INFO("slotPreviousVertexButtonClicked()");
  mTopology->previousVertex();
  mVTKView->update();
}

void meshinfo::slotNextVertexButtonClicked()
{
  INFO("slotNextVertexButtonClicked()");
  mTopology->nextVertex();
  mVTKView->update();
}

void meshinfo::slotLastVertexButtonClicked()
{
  INFO("slotLastVertexButtonClicked()");
  mTopology->lastVertex();
  mVTKView->update();
}

///////////////////////////////////////////////////////////////////////////////
// Element navigation                                                        //
///////////////////////////////////////////////////////////////////////////////

void meshinfo::slotElementNavigationGroupBoxClicked(bool checked)
{
  INFO("slotElementNavigationGroupBoxClicked()");
  if (mCurrentActiveNavigationGroupBox != mElementNavigationGroupBox) {
    INFO("   not currently active... activating");
    mCurrentActiveNavigationGroupBox->setChecked(false);
    mCurrentActiveNavigationGroupBox = mElementNavigationGroupBox;

    mVertexNeighboursCheckBox->setEnabled(false);
    mElementNeighboursCheckBox->setEnabled(true);
    mBoundaryVertexNeighboursCheckBox->setEnabled(false);
    mBoundaryFaceNeighboursCheckBox->setEnabled(false);

    mTopology->switchViewMode(ELEMENT);
    mVTKView->update();
  } else {
    INFO("   is currently active... ignoring");
    mCurrentActiveNavigationGroupBox->setChecked(true);
  }
}

void meshinfo::slotElementNeighboursCheckBoxClicked(bool checked)
{
  INFO("slotElementNeighboursCheckBoxClicked()");
  mTopology->toggleElementNeighbours();
  mVTKView->update();
}

void meshinfo::slotFirstElementButtonClicked()
{
  INFO("slotFirstElementButtonClicked()");
  mTopology->firstElement();
  mVTKView->update();
}

void meshinfo::slotPreviousElementButtonClicked()
{
  INFO("slotPreviousElementButtonClicked()");
  mTopology->previousElement();
  mVTKView->update();
}

void meshinfo::slotNextElementButtonClicked()
{
  INFO("slotNextElementButtonClicked()");
  mTopology->nextElement();
  mVTKView->update();
}

void meshinfo::slotLastElementButtonClicked()
{
  INFO("slotLastElementButtonClicked()");
  mTopology->lastElement();
  mVTKView->update();
}

///////////////////////////////////////////////////////////////////////////////
// Boundary vertex navigation                                                //
///////////////////////////////////////////////////////////////////////////////

void meshinfo::slotBoundaryVertexNavigationGroupBoxClicked(bool checked)
{
  INFO("slotBoundaryVertexNavigationGroupBoxClicked()");
  if (mCurrentActiveNavigationGroupBox != mBoundaryVertexNavigationGroupBox) {
    INFO("   not currently active... activating");
    mCurrentActiveNavigationGroupBox->setChecked(false);
    mCurrentActiveNavigationGroupBox = mBoundaryVertexNavigationGroupBox;

    mVertexNeighboursCheckBox->setEnabled(false);
    mElementNeighboursCheckBox->setEnabled(false);
    mBoundaryVertexNeighboursCheckBox->setEnabled(true);
    mBoundaryFaceNeighboursCheckBox->setEnabled(false);
  } else {
    INFO("   is currently active... ignoring");
    mCurrentActiveNavigationGroupBox->setChecked(true);
  }
}

void meshinfo::slotBoundaryVertexNeighboursCheckBoxClicked(bool checked)
{
  INFO("slotBoundaryVertexNeighboursCheckBoxClicked()");
}

void meshinfo::slotFirstBoundaryVertexButtonClicked()
{
  INFO("slotFirstBoundaryVertexButtonClicked()");
}

void meshinfo::slotPreviousBoundaryVertexButtonClicked()
{
  INFO("slotPreviousBoundaryVertexButtonClicked()");
}

void meshinfo::slotNextBoundaryVertexButtonClicked()
{
  INFO("slotNextBoundaryVertexButtonClicked()");
}

void meshinfo::slotLastBoundaryVertexButtonClicked()
{
  INFO("slotLastBoundaryVertexButtonClicked()");
}

///////////////////////////////////////////////////////////////////////////////
// Boundary face navigation                                                  //
///////////////////////////////////////////////////////////////////////////////

void meshinfo::slotBoundaryFaceNavigationGroupBoxClicked(bool checked)
{
  INFO("slotBoundaryFaceNavigationGroupBoxClicked()");
  if (mCurrentActiveNavigationGroupBox != mBoundaryFaceNavigationGroupBox) {
    INFO("   not currently active... activating");
    mCurrentActiveNavigationGroupBox->setChecked(false);
    mCurrentActiveNavigationGroupBox = mBoundaryFaceNavigationGroupBox;

    mVertexNeighboursCheckBox->setEnabled(false);
    mElementNeighboursCheckBox->setEnabled(false);
    mBoundaryVertexNeighboursCheckBox->setEnabled(false);
    mBoundaryFaceNeighboursCheckBox->setEnabled(true);
  } else {
    INFO("   is currently active... ignoring");
    mCurrentActiveNavigationGroupBox->setChecked(true);
  }
}

void meshinfo::slotBoundaryFaceNeighboursCheckBoxClicked(bool checked)
{
  INFO("slotBoundaryFaceNeighboursCheckBoxClicked()");
}

void meshinfo::slotFirstBoundaryFaceButtonClicked()
{
  INFO("slotFirstBoundaryFaceButtonClicked()");
}

void meshinfo::slotPreviousBoundaryFaceButtonClicked()
{
  INFO("slotPreviousBoundaryFaceButtonClicked()");
}

void meshinfo::slotNextBoundaryFaceButtonClicked()
{
  INFO("slotNextBoundaryFaceButtonClicked()");
}

void meshinfo::slotLastBoundaryFaceButtonClicked()
{
  INFO("slotLastBoundaryFaceButtonClicked()");
}

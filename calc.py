from sympy.vector import CoordSysCartesian
from sympy import *

mP00 = symbols('mP0[0]')
mP01 = symbols('mP0[1]')
mP02 = symbols('mP0[2]')

mP10 = symbols('mP1[0]')
mP11 = symbols('mP1[1]')
mP12 = symbols('mP1[2]')

mP20 = symbols('mP2[0]')
mP21 = symbols('mP2[1]')
mP22 = symbols('mP2[2]')

N = CoordSysCartesian('N')

v0 = mP00*N.i + mP01*N.j + mP02*N.k
v1 = mP10*N.i + mP11*N.j + mP12*N.k
v2 = mP20*N.i + mP21*N.j + mP22*N.k

n = (v1-v0).cross(v2-v1)

print(n.dot(N.i))
print(n.dot(N.j))
print(n.dot(N.k))
